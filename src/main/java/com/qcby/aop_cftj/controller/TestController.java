package com.qcby.aop_cftj.controller;

import com.qcby.aop_cftj.myannotation.NoRepeatSubmit;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

@Slf4j
@RequestMapping("/hello")
@RestController
public class TestController {

    @Autowired
    LoginService loginService;

    @RequestMapping("/login")
    @NoRepeatSubmit
    public String redisset(String phone, String password,HttpServletResponse response){
        return loginService.login(phone,password,response);
    }
}
